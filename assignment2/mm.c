/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  mm.c
 *
 *  This file contains the functions and structs for the memory management
 *
 */

#include <stdio.h>
#include <stdint.h>

#include "myalloc.h"
#include "mm.h"

/*****************************************************************************/


struct mm_state
{
    header_t *start_h;       // Init. header of current module
    header_t *recent_free_h; // Header that is left of most recent free'd alloc

    char *base;              // Base of the current module
    size_t nbanks;           // Handig
    size_t nbanks_taken;     // Number of banks taken by management
    size_t bank_size;        // Handig
    signed char *bank_state;  // Keeps track of active banks

};


struct header
{
    size_t size;    // Size in bytes including header size;
    header_t *prev; // Previous header
    header_t *next; // Next header
};


inline size_t max(size_t a, size_t b) { return a > b ? a : b; }
inline size_t min(size_t a, size_t b) { return a < b ? a : b; }


/* Create a header given an address, a size (ex. header),
 * a previous header, and a next header */
header_t *make_header(void* addr, size_t nbytes,
                      header_t* prev_h, header_t* next_h)
{
    header_t *new_h = (header_t*)addr;
    new_h->size = nbytes + sizeof(header_t);
    new_h->prev = prev_h;
    new_h->next = next_h;

    return new_h;
}


/* Calculates which bank addr is in */
size_t find_bank(char* base, size_t bank_size, size_t addr)
{
    size_t rel_pos = addr - (size_t)base;

    return rel_pos / bank_size;
}


/* Activate banks from start to end in a certain module
 * given the amount of banks active */
void activate_banks(mm_state_t* state,
                    size_t start, size_t end, size_t module)
{
    printf("Activated banks %zu through %zu!\n", start, end);

    for (size_t bank = start; bank <= end; ++bank) {
        if (state->bank_state[bank] != INUSE) {
            /* Only activate if needed */
            if (state->bank_state[bank] == INACTIVE)
                hw_activate(module, bank);

            state->bank_state[bank] = INUSE;
        }
    }
}


void update_banks(mm_state_t* state,
                  size_t start, size_t end, size_t module)
{
    for (size_t bank = start; bank <= end; ++bank) {
        switch (state->bank_state[bank]) {
            /* Already deactivated: do nothing */
            case INACTIVE:
                break;
            /* Activated and in use: do nothing */
            case INUSE:
                break;
            /* Timer expired: deactivate */
            case EXPIRED:
                hw_deactivate(module, bank);
                state->bank_state[bank] = INACTIVE;
                break;
            /* Timer in progress: subtract one */
            default:
                --state->bank_state[bank];
                break;
        }
    }
}


void set_timer(mm_state_t* state, size_t start, size_t end, signed char timer)
{
    for (size_t bank = start; bank <= end; ++bank)
        if (state->bank_state[bank] == INUSE)
            state->bank_state[bank] = timer;
}


mm_state_t *mm_initialize(void)
{
    const struct ram_info *info = hw_raminfo();

    /* New bank index is independent of base;
     * Use the first (few) bank(s) to store management data
     * Amount of bytes needed for memory state, and array for state of banks */
    size_t new_bank = find_bank(0, info->bank_size,
                                sizeof(mm_state_t) +
                                sizeof(signed char) * info->nbanks_per_module);

    for (size_t bank = 0; bank <= new_bank; ++bank)
        hw_activate(MODULE, bank);

    char *base = (char*)info->module_addrs[0];

    /* Set initial state */
    mm_state_t* state = (mm_state_t*)base;
    state->base = base;
    state->start_h = state->recent_free_h = NULL;
    state->nbanks = info->nbanks_per_module;
    state->nbanks_taken = new_bank + 1;
    state->bank_size = info->bank_size;
    state->bank_state = (signed char*)base + sizeof(mm_state_t);

    /* Set which banks are active / inactive */
    for (size_t i = 0; i < state->nbanks; ++i)
        state->bank_state[i] = (i <= new_bank ? INUSE : INACTIVE);

    if (VERBOSE) {
        printf("BANKS: %zu\n", info->nbanks_per_module);
        printf("BANK_SIZE: %zu\n", info->bank_size);
        printf("MM_STATE_SIZE: %zu\n", sizeof(mm_state_t));
        printf("HEADER_SIZE: %zu\n", sizeof(header_t));
        printf("BANKS TAKEN: %zu\n", new_bank + 1);
    }

    return state;
}


/* Create the first headers (when no other headers exist)
 * This function expects nbytes to be rounded to a multiple of maxint */
void* set_initial_header(mm_state_t* state, size_t bank_size, size_t nbytes)
{
    /* New bank index is independent of base */
    size_t new_bank = find_bank(0, bank_size,
                                nbytes + sizeof(header_t) +
                                (bank_size * state->nbanks_taken) + 1);

    /* If the amount of banks needed is too high,
     * the allocation failed and nullpointer must be returned */
    if (new_bank >= state->nbanks)
        return NULL;

    /* Activate required banks and update state */
    activate_banks(state, state->nbanks_taken, new_bank, MODULE);

    /* Make the header and put it in the state */
    void* new_addr = (void*)state->base + (state->nbanks_taken * bank_size);
    header_t *new_h = make_header(new_addr, nbytes, NULL, NULL);

    state->start_h = new_h;

    return (void*)new_h + sizeof(header_t);
}


/* Attempt to fit alloc, return NULL on failure */
void* attempt_insertion(mm_state_t* state, size_t nbytes, header_t* current_h)
{
    header_t* next_h = current_h->next;
    size_t space = (size_t)next_h - ((size_t)(current_h) + current_h->size);

    /* Not enough space */
    if (space < nbytes + sizeof(header_t))
        return NULL;

    /* Activate the required banks */
    size_t l_bank = find_bank(state->base, state->bank_size,
                              (size_t)current_h + current_h->size);
    size_t r_bank = find_bank(state->base, state->bank_size,
                              (size_t)next_h);
    size_t t_bank = find_bank(state->base, state->bank_size,
                              (size_t)current_h + current_h->size + nbytes +
                              sizeof(header_t));
    activate_banks(state, l_bank + 1, min(t_bank, r_bank - 1), MODULE);

    /* Make header after alloc'ed block of current that points
     * to the header that current points to */
    void* new_addr = (void*)((size_t)current_h + current_h->size);
    header_t *new_h = make_header(new_addr, nbytes, current_h, next_h);

    /* Make the neighbouring headers point to the new header */
    current_h->next = new_h;
    next_h->prev = new_h;

    return (void*)new_h + sizeof(header_t);
}


void* attempt_append(mm_state_t* state, size_t bank_size,
                     size_t nbytes, header_t* current_h)
{
    printf("ATTEMPTING APPEND\n");

    size_t current_bank = find_bank(state->base, bank_size,
                                    (size_t)current_h + current_h->size);
    size_t tail = (size_t)current_h +
                   current_h->size  +
                   sizeof(header_t) + nbytes;
    size_t new_bank = (tail - (size_t)state->base) / bank_size;

    /* If the amount of banks needed is too high,
     * the allocation failed and nullpointer must be returned */
    if (new_bank >= state->nbanks)
        return NULL;

    /* Activate required banks */
    activate_banks(state, current_bank + 1, new_bank, MODULE);

    /* Make the new header and link up current header */
    void* new_addr = (void*)((size_t)current_h + current_h->size);
    header_t *new_h = make_header(new_addr, nbytes, current_h, NULL);
    current_h->next = new_h;

    return (void*)new_h + sizeof(header_t);
}


void *mm_alloc(mm_state_t* state, size_t nbytes)
{
    printf("\nAlloc was called for %zu bytes.\n", nbytes);

    /* Update all banks */
    update_banks(state, 0, state->nbanks - 1, MODULE);

    /* Round to next multiple of intmax_t for alignment */
    nbytes += sizeof(intmax_t) - (nbytes % sizeof(intmax_t));

    header_t* current_h = state->start_h;
    void* ptr;

    /* No headers yet: make the first one */
    if (current_h == NULL)
        return set_initial_header(state, state->bank_size, nbytes);

    /* Try to insert at last deallocated bank */
    if (state->recent_free_h != NULL &&
        state->recent_free_h->next != NULL &&
        (ptr = attempt_insertion(state, nbytes, state->recent_free_h)) != NULL)
        return ptr;

    /* Else loop through headers to find a spot, stop at the last one */
    while (current_h->next != NULL) {
        if((ptr = attempt_insertion(state, nbytes, current_h)) != NULL)
            return ptr;

        current_h = current_h->next;
    }

    /* If there is no room between two headers, try to append */
    return attempt_append(state, state->bank_size, nbytes, current_h);
}


/* Unlinks a header, essentially "forgetting" that it exists, so the
 * memory can be reused */
void unlink_header(header_t* current_h)
{
    header_t* left_h = current_h->prev;
    header_t* right_h = current_h->next;

    /* Header is in the middle, neither at start nor end */
    if (left_h != NULL && right_h != NULL) {
        left_h->next = right_h;
        right_h->prev = left_h;
    }
    else if (left_h != NULL && right_h == NULL) {
        left_h->next = NULL;
    }
    /* Do not remove initheader, instead leave it be with only its own
     * size contributing */
    else if (left_h == NULL) {
        current_h->size = sizeof(header_t);
    }
}


/* Free an amount of space. We should never */
void mm_free(mm_state_t* state, void *ptr)
{
    /* Find the headers associated to this pointer */
    header_t* current_h = (header_t*)(ptr - sizeof(header_t));
    header_t* left_h = current_h->prev;
    header_t* right_h = current_h->next;

    /* The left bank index is exactly the amount of banks used for
     * management if its the first one */
    size_t l_bank = (left_h == NULL ? state->nbanks_taken :
                     find_bank(state->base, state->bank_size,
                               (size_t)left_h + left_h->size));

    /* The right bank index cannot be tracked if this header is the last one,
     * so just assign last bank in that case*/
    size_t r_bank = (right_h == NULL ? state->nbanks :
                     find_bank(state->base, state->bank_size,
                               (size_t)right_h));

    /* Set the timer for all banks possibly not in use, and let
     * banks unused at the end of the line be deactivated immediately */
    signed char timer = (right_h == NULL ? EXPIRED : TIMER);
    set_timer(state, l_bank + 1, r_bank - 1, timer);

    unlink_header(current_h);

    /* Update the pointer to the most recent free */
    state->recent_free_h = left_h;

    /* Update all banks */
    update_banks(state, 0, state->nbanks - 1, MODULE);
}
