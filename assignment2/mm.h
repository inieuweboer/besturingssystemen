/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  mm.h
 *
 */


/*****************************************************************************/

#ifndef MM_H
#define MM_H


#define VERBOSE 0

#define MODULE 0

#define INACTIVE 0
#define INUSE 1
#define EXPIRED 2

/* TIMER *HAS* to be >= EXPIRED */
#define TIMER 10


struct header;
typedef struct header header_t;

size_t max(size_t, size_t);
size_t min(size_t, size_t);

header_t *make_header(void* addr, size_t nbytes,
                      header_t* prev, header_t* next);
size_t find_bank(char* base, size_t bank_size, size_t addr);
void print_address(header_t* h, char* base);

void activate_banks(mm_state_t*, size_t start, size_t end, size_t module);
void update_banks  (mm_state_t*, size_t start, size_t end, size_t module);
void set_timer     (mm_state_t*, size_t start, size_t end, signed char timer);


void* set_initial_header(mm_state_t*, size_t bank_size, size_t nbytes);
void* attempt_insertion(mm_state_t*, size_t nbytes, header_t* current);
void* attempt_append(mm_state_t*, size_t bank_size,
                     size_t nbytes, header_t* current);

void unlink_header(header_t*);

#endif /* MM_H */

