#include "sync.h"

mutex_t *alloc_mutex(void)
{
    return (void*) 1;
}

void free_mutex(mutex_t* m)
{
    (void)m;
}

void mutex_lock(mutex_t* m)
{
    (void)m;
}

int mutex_try_lock(mutex_t* m)
{
    (void)m;
    return 0;
}

void mutex_unlock(mutex_t* m)
{
    (void)m;
}
