#include "philosophers.h"
#include "sync.h"

void do_eat(size_t p, size_t n, mutex_t* left_fork, mutex_t* right_fork)
{
    size_t i;

    i = 0;
    while (i < n)
    {
        mutex_lock(left_fork);

        if (mutex_try_lock(right_fork) != 0)
        {
            mutex_unlock(left_fork);
            continue;
        }

        use_left(p);
        use_right(p);

        mutex_unlock(left_fork);
        mutex_unlock(right_fork);
        ++i;
    }
}


void prepare(size_t np, size_t p)
{
    /* nothing here... */
    (void)np;
    (void)p;
}
