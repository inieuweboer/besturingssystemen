#ifndef PHILOSOPHERS_H
#define PHILOSOPHERS_H

#include <stddef.h>
#include "sync.h"

void use_left(size_t p);
void use_right(size_t p);

void prepare(size_t np, size_t p);
void do_eat(size_t p, size_t n, mutex_t* left_fork, mutex_t* right_fork);


#endif
