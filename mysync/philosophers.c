#include "philosophers.h"
#include "sync.h"
#include <sys/time.h>
#include <pthread.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#define pthread(Function, ...) do { if (pthread_ ## Function(__VA_ARGS__)) perror("pthread_" #Function); } while(0)

static mutex_t** forks = 0;
static volatile size_t* use_counters = 0;
static pthread_t* table = 0;
static float* eating_times = 0;
static unsigned np = 4;
static unsigned n = 100000;

void use_left(size_t p)
{
    volatile size_t *left_counter = &use_counters[p];
    (*left_counter)++;
}
void use_right(size_t p)
{
    volatile size_t *left_counter = &use_counters[(p + 1) % np];
    (*left_counter)++;
}

static
void* behavior(void *arg)
{
    size_t p = (size_t) arg;
    mutex_t *left = forks[p];
    mutex_t *right = forks[(p + 1) % np];
    struct timeval tv1, tv2;

    gettimeofday(&tv1, 0);
    do_eat(p, n, left, right);
    gettimeofday(&tv2, 0);
    eating_times[p] = (float)(tv2.tv_sec - tv1.tv_sec) + 1e-6 * ((float)tv2.tv_usec - (float)tv1.tv_usec);
    return 0;
}

static
void make_forks(void)
{
    size_t i;

    forks = malloc(np * sizeof(forks[0]));
    assert(forks != 0);
    use_counters = calloc(np, sizeof(use_counters[0]));
    assert(use_counters != 0);

    for (i = 0; i < np; ++i)
    {
        forks[i] = alloc_mutex();
        assert(forks[i] != 0);
    }
}

static
void invite_philosophers(void)
{
    size_t i;

    table = malloc(np * sizeof(table[0]));
    assert(table != 0);
    eating_times = calloc(np, sizeof(eating_times[0]));
    assert(eating_times != 0);

    for (i = 0; i < np; ++i)
        pthread(create, &table[i], 0, &behavior, (void*) i);
}

static
void wait_for_dinner(void)
{
    size_t i;

    for (i = 0; i < np; ++i)
        pthread(join, table[i], 0);
}

static
void run_dinner_table(void)
{
    struct timeval tv1, tv2;
    float total_time;
    size_t i;
    bool ok;

    make_forks();

    printf("starting dinner party...\n");

    gettimeofday(&tv1, 0);
    invite_philosophers();
    wait_for_dinner();
    gettimeofday(&tv2, 0);

    printf("end of dinner party!\n");
    total_time = tv2.tv_sec - tv1.tv_sec + 1e-6 * ((float)tv2.tv_usec - (float)tv1.tv_usec);

    printf("total time: %.3fms\n", 1000 * total_time);
    printf("avg time per dish: %.3fus\n", 1e6 * total_time / n);
    printf("stats per philosopher: index, total time (ms), time per dish (us)\n");
    for (i = 0; i < np; ++i)
        printf("%zu %.3f %.3f\n", i, eating_times[i] * 1000, 1e6 * eating_times[i] / n);

    for (ok = true, i = 0; i < np; ++i)
        if (use_counters[i] != 2 * n)
        {
            printf("error: fork %zu was used %zu times\n", i, use_counters[i]);
            ok = false;
        }
    if (!ok)
        exit(1);
}

int main(int argc, char **argv)
{
    int opt;

    while ((opt = getopt(argc, argv, "hp:n:")) != -1)
        switch (opt) {
        case 'h':
            printf("usage: %s [OPTS]\n"
                   "options:\n"
                   " -h      print this help.\n"
                   " -p N    invite N philosophers for dinner (default 4).\n"
                   " -n N    serve N dishes (default 100000).\n",
                   argv[0]);
            return EXIT_SUCCESS;
        case 'p':
            np = strtoul(optarg, 0, 0);
            break;
        case 'n':
            n = strtoul(optarg, 0, 0);
            break;
        }

    assert(np >= 2);

    run_dinner_table();
    return 0;
}
