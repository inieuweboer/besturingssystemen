#ifndef SYNC_H
#define SYNC_H

struct my_mutex;
typedef struct my_mutex mutex_t;

mutex_t* alloc_mutex(void);
void free_mutex(mutex_t* m);

void mutex_lock(mutex_t* m);

// must return 0 if successful, 1 if already locked
int mutex_try_lock(mutex_t *m);

void mutex_unlock(mutex_t* m);

#endif
