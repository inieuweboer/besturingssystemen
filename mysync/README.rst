======================
 Assignment 4: MySync
======================

:Date: May 16th, 2015
:Deadline: May 24th, 2015

Objectives
==========

You must implement a locking API in three or more different ways and
compare their performance.

.. note:: The programming part of this assignment can be very short
   (e.g. 1-2 hours), this is OK. The difficulty will be reading the
   documentation necessary before the programming starts (possibly a
   couple of hours), and after the programming effort to measure
   performance, producing plots and explaining them. 

Context
=======

Your work will be evaluated in the context of a known solution to the
"dining philosophers problem".

In this problem, a group of NP philosophers is invited to a circular
dinner table. Each philosopher must eat N dishes.  Each dish must be
eaten with 2 forks: a philosopher can only start eating after he/she
has grabbed 2 forks left and right. However there are only P forks
available, one between each philosopher. The problem exists because if
each philosopher always grabs the left fork and then waits to grab
the right fork, the dinner can be blocked ("deadlocked") with every
philosopher holding a fork in their left hand and waiting for their
right fork to become available.

There are many solutions to this problem. The solution proposed as
framework for this assignment is called "optimistic back-off": each
philosophers grabs the left fork, then *attempts* to grab the right
fork. If this succeeds, then the philosopher proceeds to eat as
usual; however if grabbing the right fork fails, the philosopher puts
the left fork back down and then waits a small amount of time.

This philosopher algorithm is already implemented (see ``behavior.c``)
but it requires a working implementation of a *locking API*: three
primitives "lock", "try lock" and "unlock", to protect against
simultaneous use of the same fork by more than one philosopher.

Your work will be to implement this locking API and measure how
fast the philosophers eat using your API.


Getting started
===============

1. unpack the provided source code archive; then run ``make``
2. Try out the generated ``filo_none`` and familiarize yourself with
   its interface (``-h``). Notice how ``filo_none`` uses the implementation
   in ``sync_none.c``.
3. Copy the file ``sync_none.c`` to ``sync_simple.c``, then run
   ``make`` again. Notice how ``filo_simple`` is compiled to use
   ``sync_simple.c`` automatically.
4. Edit the file ``sync_simple.c``.
   This is probably where you should start implementing your locking API.

.. raw:: pdf

   PageBreak

Requirements
============

Your code must not use global variables; the only storage you can use
for your API is the data you allocate in your ``alloc_mutex``
function. You must submit your work as a tarball [#]_. Your archive
must also contain:

- a text file file named “``AUTHORS``” containing your name(s) and
  Student ID(s).
- a file named ``report.pdf`` which contains a write-up of your
  results (see below).

You may work in groups of 2.  **It is strongly advised you use version
control.**

.. [#] http://lmgtfy.com/?q=how+to+make+a+tarball

Description of the functions to implement
=========================================

``alloc_mutex()``
   Allocate a mutual exclusion lock for the requesting program.
   Return a pointer to the lock, or 0 if the lock could not be allocated.

``free_mutex(p)``
   Free the mutual exclusion lock referred to by ``p``.

``mutex_lock(p)``
   Perform the lock operation. If the lock is already taken,
   wait until the lock is released then take it.

``mutex_unlock(p)``
   Perform the unlock operation.

``mutex_try_lock(p)``
   Attempt to perform the lock operation. If the lock is
   already taken, return 1; otherwise take the lock and return 0.

Grading
=======

Your grade starts from 0, and the following tests determine your grade
(in no particular order):

- +0,5pt if you have submitted an archive in the right format with an
  ``AUTHORS`` file and a non-empty ``report.pdf``.
- +0,5pt if your source code builds without errors and you have
  created any ``sync_xxx.c`` files besides ``sync_none.c``.
- +2pt if your report satisfies the requirements set below.
- +0,5pt per correct locking API implementation (code), +0,5pt per
  implementation also measured and documented in your report
  (see below).
- -1pt if *either* ``gcc -W -Wall`` or ``clang -W -Wall`` reports
  warnings when compiling your code.
- -1pt if ``valgrind`` complains about your code.
- +1pt overall if you have produced and compared your measurements
  over two or more operating systems, and are able to explain the
  differences.

.. raw:: pdf

   PageBreak

Multiple locking APIs
=====================

Your must provide at least 3 different implementations of the locking
API in different files ``sync_xxx.c``. You can invent your own
implementation, and/or combine any of the following:

- using ``pthread_mutex_lock`` (POSIX locks);
- using ``sem_wait`` / ``sem_post`` (POSIX semaphores);
- using ``semop`` (System V semaphores);
- using ``mkdir`` (lock = wait until directory creation successful,
  unlock = remove directory);
- using ``pthread_spin_lock`` (Linux spinlocks);
- your own ISO C11-compliant spinlocks using primitives in ``stdatomic.h``;
- using ``mq_send`` / ``mq_receive`` (POSIX message queues).

Structure of your report
========================

The accompanying ``report.pdf`` must contain at least the following:

- 0,5pt: your name(s) and student ID(s);
- 0,5pt: a table which compares the performance of all your implementations
  for a dinner of 4 philosophers and another for 200 philosophers;
- 0,5pt: for each execution platform where you perform your measurements, a
  detailed description of the platform (processor model, frequency,
  number of cores, memory) and OS specifications;
- for each implementation (0,5pt per implementation):

  - a short explanation of how the implementation works;
  - plots of the average, median and standard deviation of the
    per-philosopher, per-dish processing time for table sizes between
    2 and 200 (x axis = number of philosophers, y axis = value).
- 0,5pt: a discussion section where you explain which
  implementation(s) perform the best at various table sizes. Are there
  any implementations that are better at small table sizes and worse
  at large table sizes? Why? Are there implementations that appear to
  be always disadvantageous?

It is also possible to group the plots differently, for example one
combined plot for the average of all implementations, another for the
median, and another for the standard deviation, instead of one plot
per implementation. Choose whichever representation best supports your
arguments.
  
