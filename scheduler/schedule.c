/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  schedule.c
 *
 *  Code for scheduling memory and CPU time for processes.
 *
 *  Author of framework:
 *  G.D. van Albada
 *  Section Computational Science
 *  Universiteit van Amsterdam
 *  Date:
 *  October 23, 2003
 *  Modified:
 *  September 29, 2005
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "schedule.h"
#include "mem_alloc.h"


/****************************************************************************/

/* This variable will simulate the allocatable memory */
static long memory[MEM_SIZE];


/* Print a queue of processes with their id and priority. */
void print_queue(pcb* queue)
{
    while (queue) {
        printf("%li(%d) ", queue->proc_num, *((int*)queue->your_admin));
        queue = queue->next;
    }
    printf("\n");
}


/* Simple round robin */
static void move_first_to_back()
{
    /* Get last pcb */
    pcb* last_pcb = ready_proc;
    while (last_pcb->next)
        last_pcb = last_pcb->next;

    /* Only move if ready_proc not just one element */
    if (ready_proc != last_pcb) {
        pcb* first_pcb = ready_proc;
        /* Move up in queue */
        ready_proc = ready_proc->next;

        /* Sever link */
        ready_proc->prev = NULL;
        first_pcb->next  = NULL;

        /* Make link from last to first */
        last_pcb->next = first_pcb;
        first_pcb->prev = last_pcb;
    }
}


/* Insert a process after another given process of a queue */
static void insert_after(pcb* new_pcb, pcb* after_pcb)
{
    /* Set up link in front of new_pcb */
    if (after_pcb->next)
        (after_pcb->next)->prev = new_pcb;

    new_pcb->next = after_pcb->next;

    /* Set up link in back of new_pcb */
    after_pcb->next = new_pcb;
    new_pcb->prev = after_pcb;
}


/* This function is run in case of time interrupt */
static void move_first_to_back_next_queue()
{
    /* Which queue the current process is in */
    int* current_level_ptr = (int*)ready_proc->your_admin;

    /* If at base level, we can't do much more than RR */
    if (*current_level_ptr == 0) {
        move_first_to_back();
        return;
    }

    /* Otherwise decrement and move down */
    --(*current_level_ptr);

    /* Get last pcb of next queue, assuming they are ordered.
     * Note that the current level is already decremented;
     * Either stop at end of line, or when level of next pcb is lower */
    pcb* after_pcb = ready_proc;
    while (after_pcb->next &&
           *current_level_ptr <= *((int*)after_pcb->next->your_admin))
        after_pcb = after_pcb->next;

    /* Only move first pcb if it makes a difference */
    if (ready_proc != after_pcb) {
        pcb* current_pcb = ready_proc;

        /* Move up in queue */
        ready_proc = ready_proc->next;

        /* Sever link */
        ready_proc->prev  = NULL;

        /* Make link from last pcb of queue to first */
        insert_after(current_pcb, after_pcb);
    }
}


/* After process is done with IO and put in the back of ready_proc
 * by the simulator, it needs to be put in the right queue again */
static void move_after_ready_event()
{
    /* Since a process has just finished IO we know ready_proc is not empty */
    pcb* last_pcb = ready_proc;
    while (last_pcb->next)
        last_pcb = last_pcb->next;

    /* Only move pcb if that makes a difference */
    if (ready_proc == last_pcb)
        return;

    /* Get level of last pcb */
    int* current_level_ptr = (int*)last_pcb->your_admin;

    /* Get last pcb of queue of current level, assuming they are ordered
     * up until the last one.
     * Either stop at end of line, or when level of next pcb is lower */
    pcb* after_pcb = ready_proc;
    while (after_pcb->next &&
           *current_level_ptr <= *((int*)after_pcb->next->your_admin))
        after_pcb = after_pcb->next;

    /* Only move if the level is actually lower, and we're not trying
     * to move the pcb behind itself */
    if (*current_level_ptr < *((int*)after_pcb->your_admin) &&
        last_pcb != after_pcb) {

        /* Sever link */
        (last_pcb->prev)->next = NULL;

        /* Make link from last pcb of queue to first */
        insert_after(last_pcb, after_pcb);
    }
}


static void set_pcb_level(pcb* new_pcb, int level)
{
    new_pcb->your_admin = malloc(sizeof(int));
    if (new_pcb->your_admin)
        *((int*)new_pcb->your_admin) = level;
}


/* The high-level memory allocation scheduler is implemented here */
/* Move a process in new_queue to the end of ready_queue */
void move_to_ready(pcb* new_ready, int n_queues)
{
    /* Specifies which queue level this pcb is in; the topmost */
    set_pcb_level(new_ready, n_queues - 1);

    /* Clean from old queue */
    if (new_ready->prev)
        (new_ready->prev)->next = new_ready->next;
    /* Only pcb in queue... */
    else
        new_proc = new_ready->next;

    /* Clean reference from next pcb */
    if (new_ready->next)
        (new_ready->next)->prev = new_ready->prev;

    /* new_queue empty */
    if (!ready_proc) {
        ready_proc = new_ready;
        ready_proc->next = NULL;
        ready_proc->prev = NULL;
    }
    /* new_queue not empty */
    else {
        /* Find last pcb of highest level */
        pcb* after_pcb = ready_proc;
        while (after_pcb->next &&
               n_queues - 1 == *((int*)after_pcb->next->your_admin))
            after_pcb = after_pcb->next;

        /* Put it behind that one */
        insert_after(new_ready, after_pcb);
    }
}


/* After a given time period, reset all priorities to the highest level. */
void reset_queues(int n_queues, long reset_time)
{
    /* Only reset if not empty */
    if (!ready_proc)
        return;

    static int is_reset = 0;

    /* Don't know whether certain time frames are skipped, so take a range of
     * about a tenth of the reset time (milliseconds) */
    if (is_reset &&
        (long)sim_time() % reset_time > (reset_time * 9.0) / 10.0 &&
        (long)sim_time() % reset_time <= reset_time) {

        is_reset = 1;

        pcb* conductor_pcb = ready_proc;
        while(conductor_pcb->next) {
            *((int*)conductor_pcb->your_admin) = n_queues - 1;
            conductor_pcb = conductor_pcb->next;
        }

    }
    else
        is_reset = 0;

}


static void give_memory(int n_try, int n_queues)
{
    int index;
    int loc = 0;
    pcb* new_proc_try = new_proc; // The process we attempt to give memory
    pcb* temp;

    while (new_proc_try) {
        index = mem_get(new_proc_try->MEM_need);
        /* If memory allocation succeeded, set pointer and
         * move the process to the ready_queue */
        if (index >= 0) {
            temp = new_proc_try->next;
            new_proc_try->MEM_base = index;
            move_to_ready(new_proc_try, n_queues);
            new_proc_try = temp;
        }
        /* If there's no memory available, try for the
         * next element, but stop after n_try times. */
        else if (loc < n_try) {
            new_proc_try = new_proc_try->next;
            ++loc;
        }
        else
            break;
    }
}


/* Here we reclaim the memory of a process after it
  has finished */
static void reclaim_memory()
{
    pcb *proc;

    proc = defunct_proc;
    while (proc) {
        /* Free your own administrative structure if it exists
         */
        if (proc->your_admin)
            free(proc->your_admin);

        /* Free the simulated allocated memory
         */
        mem_free(proc->MEM_base);
        proc->MEM_base = -1;

        /* Call the function that cleans up the simulated process
         */
        rm_process(&proc);

        /* See if there are more processes to be removed
         */
        proc = defunct_proc;
    }
}


/* No last word, but kept in case it is needed. */
static void my_finale() {}


/* The actual CPU scheduler is implemented here */
static void CPU_scheduler(
        event_type event,
        double default_slice, int n_queues, long reset_time)
{
    /* Insert the code for a MLFbQ scheduler here */
    switch (event)
    {
    /* When a job runs out of time, move it down one queue. */
    case Time_event:
        if (ready_proc)
            move_first_to_back_next_queue();

    /* In case of a ready event, put the last event where it belongs */
    case Ready_event:
        if (ready_proc)
            move_after_ready_event();
        break;

    /* Do nothing when a program starts doing IO or an event finished. */
    case IO_event:
    case Finish_event:
        break;

    default:
        printf("I cannot handle event nr. %d\n", event);
        break;
    }

    /* Slice depends on level of first job */
    int level = n_queues - 1;
    if (ready_proc)
        level = *((int*)ready_proc->your_admin);

    /* Base level gets the longest time. */
    set_slice(default_slice * pow(2, (n_queues - level)));

    /* Reset queue levels (after some given time */
    reset_queues(n_queues, reset_time);
}


/* There is little to no protection for misuse of scanf or wrong input. */
double get_input(char* string)
{
    char dummy;

    printf("%s", string);
    double input = 1;
    int rv = scanf ("%lf", &input);
    /* skip invalid input */
    if (rv < 1)
        rv = scanf("%c", &dummy);
    printf ("Gelezen waarde: %f (wordt gecast)\n", input);

    return input;
}


/* The main scheduling routine */
void schedule(event_type event)
{
    /* Input vars */
    static int n_try = 10;
    static double default_slice = 50.0;
    static int n_queues = 5;
    static long reset_queues = 1000;

    /* Only do this on first function run */
    static int first = 1;
    if (first) {
        printf("This is the default (framework) implementation");
        mem_init(memory);
        finale = my_finale;
        first = 0;

        printf ("Invoer voor high-level en CPU scheduler.\n");
        n_try = (int)get_input("Geef gewenste waarde voor n_try\n");
        default_slice = get_input(
                "Geef gewenste time slice voor base queue\n");
        n_queues = (int)get_input("Geef gewenste aantal queues voor MLFbQ\n");
        reset_queues = (long)get_input(
                "Geef gewenste tijdsperiode voor queue reset\n");
    }

    switch (event)
    {
    /* Give memory in case of a new process */
    case NewProcess_event:
        give_memory(n_try, n_queues);
        break;

    /* Schedule in these cases */
    case Time_event:
    case Ready_event:
    case IO_event:
        CPU_scheduler(event, default_slice, n_queues, reset_queues);
        break;

    /* Reclaim, and then try to assign memory again. Then schedule. */
    case Finish_event:
        reclaim_memory();
        give_memory(n_try, n_queues);
        CPU_scheduler(event, default_slice, n_queues, reset_queues);
        break;

    default:
        printf("I cannot handle event nr. %d\n", event);
        break;
    }
}

