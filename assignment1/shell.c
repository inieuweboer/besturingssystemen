/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  shell.c
 *
 *  Code for running shell commands is implemented here.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "shell.h"

#include "ast.h"
#include "xalloc.h"


/***** Signal handling and initialization *****/

/* Is called whenever a signal is received */
void sig_handler(int signo)
{
    /* Just skip a Ctrl-c */
    if (signo == SIGINT)
        return;
}


/* This code is ran at runtime */
void initialize(void)
{
    /* Initialize sigaction struct */
    struct sigaction sigac;
    sigaction(SIGINT, 0, &sigac);
    /* Override behaviour */
    sigac.sa_handler = &sig_handler;
    sigac.sa_flags |= SA_RESTART;
    sigaction(SIGINT, &sigac, 0);
}

/***** Builtins *****/

/* Process an exit command */
void exit_shell(node_t* node)
{
    size_t argc = node->command.argc;
    char **argv = node->command.argv;
    int return_code = EXIT_SUCCESS;

    if (argc == 2 && argv[1] != NULL)
        return_code = atoi(argv[1]);
    else if (argc > 2)
        printf("42sh: exit: Too many arguments\n");

    free_tree(node);
    exit(return_code);
}

/* Set an environment variable */
void set_env(node_t* node)
{
    size_t argc = node->command.argc;
    char **argv = node->command.argv;

    /* Split the argument into name and value */
    if (argc > 1) {
        char *dup = xstrdup(argv[1]);
        char *dupp = dup; /* Needed to free the copy after strsep */
        char *name = strsep(&dup, "=");

        /* Only set variable if an '=' is found */
        if (dup != NULL)
            setenv(name, dup, 1); /* 1: overwerite previous value */
        else
            printf("set: No '=' in argument\n");

        free(dupp);
    }
}

/* Unset an environment variable */
void unset_env(node_t* node)
{
    size_t argc = node->command.argc;
    char **argv = node->command.argv;

    if (argc > 1)
        unsetenv(argv[1]);
}


/* Process a cd command */
void choose_dir(node_t* node)
{
    size_t argc = node->command.argc;
    char **argv = node->command.argv;
    int result = 0;

    if (argc > 1) {
        result = chdir(argv[1]);
        if (result == -1) {
            printf("cd: %s: Not a directory\n", argv[1]);
        }
    }
    else {
        chdir("/");
    }
}


/* Process a command that has to link to a program */
void run_program(node_t* node)
{
    char *program = node->command.program;
    char **argv = node->command.argv;
    int result = 0;

    pid_t child_pid = 0;
    int status;

    if (!(child_pid = fork())) {
        result = execvp(program, argv);
        /* Print an error message if needed */
        if (result == -1) {
            perror(NULL);
            exit(EXIT_FAILURE);
        }
    }
    else {
        waitpid(child_pid, &status, 0);
    }
}

/***** "Simple" parsers *****/

/* Parse a single command. */
void parse_command(node_t* node)
{
    /* Check built-ins, otherwise try to run a program */
    if (strncmp(node->command.program, "exit", 100) == 0)
        exit_shell(node);

    else if (strncmp(node->command.program, "cd", 100) == 0)
        choose_dir(node);

    else if (strncmp(node->command.program, "set", 100) == 0)
        set_env(node);

    else if (strncmp(node->command.program, "unset", 100) == 0)
        unset_env(node);

    else
        run_program(node);
}


/* Parsing a command between parentheses */
void parse_subshell(node_t* node)
{
    pid_t child_pid = 0;
    int status = 0;
    /* Execute tree in new process and terminate it afterwards */
    if (!(child_pid = fork())) {
        parse_tree(node);
        exit(EXIT_SUCCESS);
    }
    else
        waitpid(child_pid, &status, 0);
}


/* Essentially a subshell we do not wait for */
void parse_detach(node_t* node)
{
    pid_t child_pid = 0;
    /* Execute tree in new process and terminate it afterwards */
    if (!(child_pid = fork())) {
        parse_tree(node);
        exit(EXIT_SUCCESS);
    }
}

/***** Piping *****/

/* Fill an int pointer with file descriptors for the pipes */
int* create_pipes(size_t npipes)
{
    int result;

    int *pipe_fd = xalloc(2*npipes * sizeof(int));

    /* Create pipes */
    for (size_t i = 0; i < npipes; ++i) {
        /* Create file descriptors for the current pipe and handle the error */
        result = pipe(pipe_fd + 2*i);
        if (result < 0) {
            perror(NULL);
            exit(EXIT_FAILURE);
        }
    }
    return pipe_fd;
}


/* Clean file descriptors by closing them all,
 * then free the corresponding pointer */
void clean_pipes(int *pipe_fd, size_t npipes)
{
    for (size_t i = 0; i < npipes; ++i)
        close(pipe_fd[i]);

    free(pipe_fd);
}


/* Given an iteration and the amount of pipes,
 * connect the file descriptors of stdin and stdout streams
 * to the corresponding file descriptors of the pipes */
void link_streams(int *pipe_fd, size_t i, size_t npipes)
{
    /*
     * Suppose there are three commands ran through two pipes: cmd0|cmd1|cmd2
     * Then four file descriptors (fds) are created, label them [0 1 2 3]
     * The key point is that each fd pair 2k,2k+1 is connected through a pipe
     * Visualization:
     * stdin  →  I     0  →  I     2  →  I
     *          cmd0   ↑    cmd1   ↑    cmd2
     *           O  →  1     O  →  3     O  →  stdout
     * This freely generalizes to an arbitrary amount of pipes.
     */

    /* Do not link stdin for first command */
    if (i != 0) {
        dup2(pipe_fd[2*(i-1)], STDIN_FILENO);
    }

    /* Do not link stdout and close for last command */
    if (i != npipes) {
        dup2(pipe_fd[2*i + 1], STDOUT_FILENO);

        /* Close unused read end of the pipe until use in next iteration*/
        close(pipe_fd[2*i]);
    }
}


/* Close non-used file descriptors in the main shell */
void close_fds(int *pipe_fd, size_t i, size_t npipes)
{
    /*
     * Using the same diagram, visualization for i == 1; X is closed
     * stdin- - -I     X- - -I     2- - -I
     *          cmd0   |    cmd1   |    cmd2
     *           O- - -1     O- - -X     O- - -stdout
     */

    /* Do not close first file descriptor for first command */
    if (i > 0)
        close(pipe_fd[2*(i-1)]);

    /* Do not close second file descriptor for last command */
    if (i < npipes)
        close(pipe_fd[2*i + 1]);
}


/* Parse pipes */
void parse_pipe(node_t* node)
{
    /* A pipe has to have at least two commands */
    size_t ncommands = node->pipe.n_parts;
    size_t npipes = ncommands - 1;

    int *pipe_fd = create_pipes(npipes);
    pid_t *child_pids = xalloc(ncommands * sizeof(pid_t));
    int status;

    for (size_t i = 0; i < ncommands; ++i) {
        /* Fork for each command */
        child_pids[i] = fork();
        if (child_pids[i] < 0) {
            perror(NULL);
            exit(EXIT_FAILURE);
        }

        /* The case of the forked process */
        else if (child_pids[i] == 0) {
            link_streams(pipe_fd, i, npipes);

            /* Parse the node present between the pipes */
            parse_tree(node->pipe.parts[i]);
            exit(EXIT_SUCCESS);
        }

        /* The case of the main shell */
        else {
            close_fds(pipe_fd, i, npipes);
        }
    }

    /* Wait for all children */
    for (size_t i = 0; i < ncommands; ++i)
        waitpid(child_pids[i], &status, 0);

    clean_pipes(pipe_fd, npipes);
    free(child_pids);
}

/***** Redirecting *****/

int set_target_fd(node_t* node)
{
    int target_fd = STDOUT_FILENO;
    switch(node->redirect.mode)
    {
    case REDIRECT_DUP:    // >&
        target_fd = node->redirect.fd2;
        break;

    case REDIRECT_INPUT:  // <
        target_fd = open(node->redirect.target, READFLAGS);
        break;

    case REDIRECT_OUTPUT: // >
        target_fd = open(node->redirect.target, WRITEFLAGS, MOD);
        break;

    case REDIRECT_APPEND: // >>
        target_fd = open(node->redirect.target, APPENDFLAGS, MOD);
        break;
    }
    return target_fd;
}


/* Parsing a redirect: >&, <, > or >> */
void parse_redirect(node_t* node)
{
    int target_fd, status;
    pid_t child_pid = 0;
    /* Execute tree in new process and terminate it afterwards */
    if (!(child_pid = fork())) {
        /* Assign file descriptor of stream to be linked */
        target_fd = set_target_fd(node);

        /* Link file descriptor to target */
        dup2(target_fd, node->redirect.fd);

        parse_tree(node->redirect.child);

        close(target_fd);
        exit(EXIT_SUCCESS);
    }
    else
        waitpid(child_pid, &status, 0);
}


/* Parse tree of commands */
void parse_tree(node_t* node)
{
    switch(node->type)
    {
    case NODE_COMMAND:
        parse_command(node);
        break;

    case NODE_SEQUENCE:
        parse_tree(node->sequence.first);
        parse_tree(node->sequence.second);
        break;

    case NODE_SUBSHELL:
        parse_subshell(node->subshell.child);
        break;

    case NODE_DETACH:
        parse_detach(node->detach.child);
        break;

    case NODE_PIPE:
        parse_pipe(node);
        break;

    case NODE_REDIRECT:
        parse_redirect(node);
        break;
    }
}


/* This function is run when a command is entered */
void run_command(node_t* node)
{
    /* Parse the entire tree. */
    parse_tree(node);

    /* Clean up the tree node. */
    free_tree(node);
}
