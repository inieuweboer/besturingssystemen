/*
 *  Ismani Nieuweboer - 10502815
 *  Lucas Slot        - 10610952
 *  DB Wiskunde en Informatica
 *
 *  shell.h
 *
 */


#ifndef SHELL_H
#define SHELL_H


typedef void sigfunc(int);

char *strsep(char**, const char*);

/* These definitions are here due to the non-standardness of signal handlers
 * and environment variables */
#define _GNU_SOURCE 1
#define _XOPEN_SOURCE 700


/* Flags for open() */
#define READFLAGS   O_RDONLY
#define WRITEFLAGS  O_WRONLY|O_CREAT|O_TRUNC
#define APPENDFLAGS O_WRONLY|O_CREAT|O_APPEND
#define MOD         S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH  // just 644 permissions


void initialize(void);

struct tree_node;

void choose_dir         (struct tree_node*);
void exit_shell         (struct tree_node*);
void run_program        (struct tree_node*);
void set_env            (struct tree_node*);
void unset_env          (struct tree_node*);

int* create_pipes   (size_t);
void clean_pipes    (int*, size_t);
void link_streams   (int*, size_t, size_t);
void close_fds      (int*, size_t, size_t);

int set_target_fd      (struct tree_node*);

void parse_tree         (struct tree_node*);
void parse_command      (struct tree_node*);
void parse_subshell     (struct tree_node*);
void parse_detach       (struct tree_node*);
void parse_pipe         (struct tree_node*);
void parse_redirect     (struct tree_node*);

void run_command        (struct tree_node*);

#endif /*SHELL_H*/
